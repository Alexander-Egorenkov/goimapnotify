module gitlab.com/shackra/goimapnotify

go 1.15

require (
	github.com/emersion/go-imap v1.0.0-beta.4.0.20190414203716-b7db4a2bc5cc
	github.com/emersion/go-imap-idle v0.0.0-20180114101550-2af93776db6b
	github.com/emersion/go-sasl v0.0.0-20200509203442-7bfe0ed36a21
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/sirupsen/logrus v1.4.1
	golang.org/x/sys v0.0.0-20190426135247-a129542de9ae // indirect
	golang.org/x/text v0.3.2 // indirect
)
